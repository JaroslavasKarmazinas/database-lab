image: golang:1.14

stages:
  - test
  - build-binary
  - build-image

test:
  stage: test
  script:
    - make test

lint:
  stage: test
  script:
    - make lint

.only_var_template: &only_tag_release
  only:
    variables:
      - $CI_COMMIT_TAG =~ /^[0-9.]+$/

.only_var_template: &only_tag_rc
  only:
    variables:
      - $CI_COMMIT_TAG =~ /^[0-9.]+[\-_]*[a-zA-Z]+[a-zA-Z0-9.\-_]*[a-zA-Z0-9]+$/

.only_var_template: &only_master
  only:
    - master

.only_var_template: &only_feature
  only:
    refs:
      - branches
    variables:
      - $CI_COMMIT_REF_SLUG != "master"
  when: manual

build-binary-alpine:
  image: golang:1.14-alpine
  stage: build-binary
  only:
    refs:
      - branches
      - tags
  artifacts:
    paths:
      - bin
  script:
    - apk add --update --no-cache make
    - make build

build-binary-client-linux:
  <<: *only_feature
  stage: build-binary
  artifacts:
    paths:
      - bin
  script:
    - go build -o bin/dblab-linux-amd64 ./cmd/cli/main.go

build-binary-client:
  <<: *only_tag_release
  stage: build-binary
  script:
    - make build-client

    # Install google-cloud-sdk.
    - echo "deb [signed-by=/usr/share/keyrings/cloud.google.gpg] http://packages.cloud.google.com/apt cloud-sdk main" | tee -a /etc/apt/sources.list.d/google-cloud-sdk.list
    - curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key --keyring /usr/share/keyrings/cloud.google.gpg add -
    - apt-get update && apt-get install -y google-cloud-sdk

    # Authenticate.
    - echo $GCP_SERVICE_KEY | gcloud auth activate-service-account --key-file=-

    # Upload artifacts.
    - gsutil -m cp -r bin/cli/* gs://database-lab-cli/${CI_COMMIT_TAG}/
    - gsutil -m cp -r bin/cli/* gs://database-lab-cli/latest/

.job_template: &build_image_definition
  image: docker:19
  stage: build-image
  artifacts:
    paths:
      - bin
  services:
    - docker:dind
  script:
    - apk update && apk upgrade && apk add --no-cache bash # TODO(anatoly): Remove dependency.
    - bash ./scripts/ci_docker_build_push.sh

build-image-feature-server:
  <<: *build_image_definition
  <<: *only_feature
  variables:
    REGISTRY_USER: "${CI_REGISTRY_USER}"
    REGISTRY_PASSWORD: "${CI_REGISTRY_PASSWORD}"
    REGISTRY: "${CI_REGISTRY}"
    DOCKER_FILE: "Dockerfile.dblab-server"
    DOCKER_NAME: "registry.gitlab.com/postgres-ai/database-lab/dblab-server"
    TAGS: "${DOCKER_NAME}:${CI_COMMIT_REF_SLUG}"

build-image-feature-client:
  <<: *build_image_definition
  <<: *only_feature
  variables:
    REGISTRY_USER: "${CI_REGISTRY_USER}"
    REGISTRY_PASSWORD: "${CI_REGISTRY_PASSWORD}"
    REGISTRY: "${CI_REGISTRY}"
    DOCKER_FILE: "Dockerfile.dblab"
    DOCKER_NAME: "registry.gitlab.com/postgres-ai/database-lab/dblab"
    TAGS: "${DOCKER_NAME}:${CI_COMMIT_REF_SLUG}"

build-image-feature-client-extended:
  <<: *build_image_definition
  <<: *only_feature
  variables:
    REGISTRY_USER: "${CI_REGISTRY_USER}"
    REGISTRY_PASSWORD: "${CI_REGISTRY_PASSWORD}"
    REGISTRY: "${CI_REGISTRY}"
    DOCKER_FILE: "Dockerfile.dblab-extended"
    DOCKER_NAME: "registry.gitlab.com/postgres-ai/database-lab/dblab-extended"
    TAGS: "${DOCKER_NAME}:${CI_COMMIT_REF_SLUG}"
  before_script:
    - cp ./bin/dblab-linux-amd64 ./bin/dblab

build-image-master-server:
  <<: *build_image_definition
  <<: *only_master
  variables:
    DOCKER_FILE: "Dockerfile.dblab-server"
    DOCKER_NAME: "registry.gitlab.com/postgres-ai/database-lab/dblab-server"
    TAGS: "${DOCKER_NAME}:master,${DOCKER_NAME}:master-${CI_COMMIT_SHORT_SHA}"

build-image-master-client:
  <<: *build_image_definition
  <<: *only_master
  variables:
    DOCKER_FILE: "Dockerfile.dblab"
    DOCKER_NAME: "registry.gitlab.com/postgres-ai/database-lab/dblab"
    TAGS: "${DOCKER_NAME}:master,${DOCKER_NAME}:master-${CI_COMMIT_SHORT_SHA}"

build-image-latest-server:
  <<: *build_image_definition
  <<: *only_tag_release
  variables:
    REGISTRY_USER: "${DH_CI_REGISTRY_USER}"
    REGISTRY_PASSWORD: "${DH_CI_REGISTRY_PASSWORD}"
    REGISTRY: "${DH_CI_REGISTRY}"
    DOCKER_FILE: "Dockerfile.dblab-server"
    DOCKER_NAME: "postgresai/dblab-server"
    TAGS: "${DOCKER_NAME}:latest,${DOCKER_NAME}:${CI_COMMIT_TAG}"

build-image-latest-client:
  <<: *build_image_definition
  <<: *only_tag_release
  variables:
    REGISTRY_USER: "${DH_CI_REGISTRY_USER}"
    REGISTRY_PASSWORD: "${DH_CI_REGISTRY_PASSWORD}"
    REGISTRY: "${DH_CI_REGISTRY}"
    DOCKER_FILE: "Dockerfile.dblab"
    DOCKER_NAME: "postgresai/dblab"
    TAGS: "${DOCKER_NAME}:latest,${DOCKER_NAME}:${CI_COMMIT_TAG}"

build-image-rc-server:
  <<: *build_image_definition
  <<: *only_tag_rc
  variables:
    REGISTRY_USER: "${DH_CI_REGISTRY_USER}"
    REGISTRY_PASSWORD: "${DH_CI_REGISTRY_PASSWORD}"
    REGISTRY: "${DH_CI_REGISTRY}"
    DOCKER_FILE: "Dockerfile.dblab-server"
    DOCKER_NAME: "postgresai/dblab-server"
    TAGS: "${DOCKER_NAME}:${CI_COMMIT_TAG}"

build-image-rc-client:
  <<: *build_image_definition
  <<: *only_tag_rc
  variables:
    REGISTRY_USER: "${DH_CI_REGISTRY_USER}"
    REGISTRY_PASSWORD: "${DH_CI_REGISTRY_PASSWORD}"
    REGISTRY: "${DH_CI_REGISTRY}"
    DOCKER_FILE: "Dockerfile.dblab"
    DOCKER_NAME: "postgresai/dblab"
    TAGS: "${DOCKER_NAME}:${CI_COMMIT_TAG}"

build-image-swagger-latest:
  <<: *build_image_definition
  <<: *only_tag_release
  variables:
    DOCKER_FILE: "Dockerfile.swagger-ui"
    DOCKER_NAME: "registry.gitlab.com/postgres-ai/database-lab/dblab-swagger-ui"
    TAGS: "${DOCKER_NAME}:latest"
